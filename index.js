const express = require("express");
const app = express();
const port = 3000;
const userRoute = require("./users/user.route");
app.get("/", (req, res) => {
  res.send("Hey this is my API running 🥳");
});

app.use("/users", userRoute);

app.listen(port, () => {
  console.log(`API listening on port ${port} `);
});

module.exports = app;
